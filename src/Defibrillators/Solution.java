package Defibrillators;

import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String LON = "3,879483".replace(',', '.');
        String LAT = "43,608177".replace(',', '.');
        int N = 3;

        ArrayList<String[]> defibList = new ArrayList<>();
        //for (int i = 0; i < N; i++) {
        defibList.add("1;Maison de la Prevention Sante;6 rue Maguelone 340000 Montpellier;;3,87952263361082;43,6071285339217".split(";"));
        defibList.add("2;Hotel de Ville;1 place Georges Freche 34267 Montpellier;;3,89652239197876;43,5987299452849".split(";"));
        defibList.add("3;Zoo de Lunaret;50 avenue Agropolis 34090 Mtp;;3,87388031141133;43,6395872778854".split(";"));
        //}

        double lonA, lonB, latA, latB, distance;
        lonA = Double.parseDouble(LON);
        latA = Double.parseDouble(LAT);
        HashMap<Double, String> distanceList = new HashMap<>();

        for (String[] s : defibList) {
            lonB = Double.parseDouble(s[s.length - 2].replace(',', '.'));
            latB = Double.parseDouble(s[s.length - 1].replace(',', '.'));
            distanceList.put(Math.sqrt(Math.pow((lonB - lonA) * (Math.cos((latA + latB) / 2)), 2) + Math.pow((latB - latA), 2)) * 6371, s[1]);
        }

//        for (Map.Entry<Double, String> pair : distanceList.entrySet()) {
//            System.out.println("Distance: " + pair.getKey() + " Location: " + pair.getValue());
//        }

        for (Map.Entry<Double, String> pair : distanceList.entrySet()) {
            if(pair.getKey()==Collections.min(distanceList.keySet())){
                System.out.println(pair.getValue());
            }

        }


    }

}
