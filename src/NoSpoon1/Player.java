package NoSpoon1;

import java.util.Scanner;

/**
 * Don't let the machines win. You are humanity's last hope...
 **/
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int width = 1; // the number of cells on the X axis
        int height = 4; // the number of cells on the Y axis

        String mainCoord = "0 0 ";
        String rightCoord = "-1 -1 ";
        String downCoord = "-1 -1";

        char[][] lines = new char[height][width];
        lines[0] = "0".toCharArray();
        lines[1] = "0".toCharArray();
        lines[2] = "0".toCharArray();
        lines[3] = "0".toCharArray();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (lines[i][j] == '0') {
                    mainCoord = String.format("%d %d ", j, i);
                    //System.out.println("MainCoord:" + mainCoord);
                    for (int k = j + 1; k < width; k++) {
                        if (j != width - 1 && lines[i][k] == '0') {
                            rightCoord = String.format("%d %d ", k, i);
                            break;
                            //System.out.println("RightCoord:" + rightCoord);
                        }
                    }
                    for (int k = i + 1; k < height; k++) {
                        if (i != height - 1 && lines[k][j] == '0') {
                            downCoord = String.format("%d %d", j, k);
                            break;
                            //System.out.println("DownCoord:" + downCoord);
                        }
                    }
                    System.out.println(mainCoord + rightCoord + downCoord);
                    rightCoord = "-1 -1 ";
                    downCoord = "-1 -1";

                }

            }

        }
    }
}