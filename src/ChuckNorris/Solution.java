package ChuckNorris;

import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String MESSAGE = "%";//in.nextLine();

        //Перевод в BINARY

        char[] chars = MESSAGE.toCharArray();
        String res = "";
        for (char c : chars) {
            String tmp = Integer.toBinaryString(c);
            if(tmp.length()<7){
                for (int i = 0; i < 7-tmp.length(); i++) {
                    tmp = "0"+ tmp;
                }
            }
            res += tmp;

        }
        System.out.println(res);

        System.out.println(convert(res));


    }

    public static String convert(String res) {
        String result = "";
        int count = 0;

        for (int i = 0; i < res.length(); i++) {
            if (res.charAt(i) == '1') {
                result += "0 0";
                for (int j = i+1; j < res.length(); j++) {
                    if (res.charAt(j) == '1') {
                        result += "0";
                        count++;
                        if(j==res.length()-1)i+=count;
                    } else {
                        result += " ";
                        i+=count;
                        count = 0;
                        j = res.length();
                    }
                }
            } else {
                result += "00 0";
                for (int j = i+1; j < res.length(); j++) {
                    if (res.charAt(j) == '0') {
                        result += "0";
                        count++;
                        if(j==res.length()-1)i+=count;
                    } else {
                        result += " ";
                        i+=count;
                        count = 0;
                        j = res.length();
                    }
                }
            }
        }

        return result;
    }
}