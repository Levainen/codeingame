package MIMEtype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt(); // Number of elements which make up the association table.
        int Q = in.nextInt(); // Number Q of file names to be analyzed.

        HashMap<String, String> table = new HashMap<>();

        for (int i = 0; i < N; i++) {
            String EXT = in.next(); // file extension
            String MT = in.next(); // MIME type.
            table.put(EXT.toLowerCase(), MT);
        }

        in.nextLine();

        ArrayList<String> fName = new ArrayList<>();
        for (int i = 0; i < Q; i++) {
            fName.add(in.nextLine()); // One file name per line.
        }

        ArrayList<String> fNameExt = new ArrayList<>();
        String extString = "";
        int count = 0;
        for (String s : fName) {
            if (s.contains(".")) {
                int dotPos = s.lastIndexOf(".");
                if (dotPos != s.length() - 1) {
                    extString = s.substring(dotPos + 1).toLowerCase();
                } else {
                    count++;
                    extString = s + count;
                }
            } else {
                count++;
                extString = s + count;
            }
            fNameExt.add(extString);
        }
//        for(String s:fNameExt) System.out.println(s);
//
//        System.out.println();
//        System.out.println();


        for(String s:fNameExt) {
            if (table.containsKey(s)) {
                for (Map.Entry<String, String> extPair : table.entrySet()) {
                    if (extPair.getKey().equals(s)) {
                        System.out.println(extPair.getValue());
                    }
                }
            } else System.out.println("UNKNOWN");
        }
        
    }
}