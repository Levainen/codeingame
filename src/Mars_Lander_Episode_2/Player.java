

import java.util.*;
import java.io.*;
import java.math.*;

import static javafx.scene.input.KeyCode.X;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {

        int landXBegin = 0;
        int landXEnd = 0;
        int landYBegin = 0;
        int landYEnd = 0;
        int maxY = 0;

        Scanner in = new Scanner(System.in);
        int surfaceN = in.nextInt(); // the number of points used to draw the surface of Mars.
        int[][] surfacePoints = new int[2][surfaceN];
        for (int i = 0; i < surfaceN; i++) {
            surfacePoints[0][i] = in.nextInt(); // X coordinate of a surface point. (0 to 6999)
            surfacePoints[1][i] = in.nextInt();// Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
            maxY = surfacePoints[1][i] > maxY ? surfacePoints[1][i] : maxY;
        }

        for (int i = 0; i < surfaceN - 1; i++) {
            if (surfacePoints[1][i] == surfacePoints[1][i + 1]) {
                landXBegin = surfacePoints[0][i];
                landYBegin = surfacePoints[1][i];
                landXEnd = surfacePoints[0][i + 1];
                landYEnd = surfacePoints[1][i + 1];
            }
        }

//        System.out.println(Arrays.deepToString(surfacePoints)+ ", " + landXBegin +" "+landYBegin+", "+ landXEnd +" "+ landYEnd + "," + maxY);

        // game loop
        while (true) {
            int X = in.nextInt();
            int Y = in.nextInt();
            int hSpeed = in.nextInt(); // the horizontal speed (in m/s), can be negative.
            int vSpeed = in.nextInt(); // the vertical speed (in m/s), can be negative.
            int fuel = in.nextInt(); // the quantity of remaining fuel in liters.
            int rotate = in.nextInt(); // the rotation angle in degrees (-90 to 90).
            int power = in.nextInt(); // the thrust power (0 to 4).

            if (X < landXBegin) {

                if (hSpeed == 0) {
                    rotate = -15;
                } else if (hSpeed > 20 && hSpeed < 30) {
                    rotate = 0;
                } else if (hSpeed > 30) {
                    rotate = 30;
                } else if (hSpeed < -20) {
                    rotate = -30;
                }

                if (vSpeed < 0 && power < 4) power += 1;
                else if (vSpeed > -40 && power > 0) power -= 1;

            } else if (X > landXEnd) {
                if (vSpeed < 0 && power < 4) power += 1;
                else if (vSpeed > -40 && power > 0) power -= 1;


                if (hSpeed == 0) {
                    rotate = 15;
                } else if (hSpeed > 20) {
                    rotate = 0;
                } else if (hSpeed < -20) {
                    rotate = -30;
                }

            } else if (X > landXBegin && X < landXEnd) {
                if (vSpeed < 0 && power < 4) power += 1;
                else if (vSpeed > -40 && power > 0) power -= 1;

                if (Y < 200+landYBegin) rotate = 0;
                else {
                    if (hSpeed == 0) {
                        rotate = 0;
                    } else if (hSpeed > 20) {
                        rotate = 15;
                    } else if (hSpeed < -20) {
                        rotate = -15;
                    }
                }

            }
            System.out.println(rotate + " " + power);
        }


    }

}