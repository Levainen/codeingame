package ASCIIArt;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    static char[] alphabet = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ?").toCharArray();

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = 4;
        int H = 5;
        //in.nextLine();
        String T = "2dfsdf".toUpperCase();
        char[] text = T.toCharArray();

        ArrayList<String> ascii = new ArrayList<String>();
//        for (int i = 0; i < H; i++) {
//            ascii.add(in.nextLine());
//        }
        ascii.add(" #  ##   ## ##  ### ###  ## # # ###  ## # # #   # # ###  #  ##   #  ##   ## ### # # # # # # # # # # ### ###  ");
        ascii.add("# # # # #   # # #   #   #   # #  #    # # # #   ### # # # # # # # # # # #    #  # # # # # # # # # #   #   #  ");
        ascii.add("### ##  #   # # ##  ##  # # ###  #    # ##  #   ### # # # # ##  # # ##   #   #  # # # # ###  #   #   #   ##  ");
        ascii.add("# # # # #   # # #   #   # # # #  #  # # # # #   # # # # # # #    ## # #   #  #  # # # # ### # #  #  #        ");
        ascii.add("# # ##   ## ##  ### #    ## # # ###  #  # # ### # # # #  #  #     # # # ##   #  ###  #  # # # #  #  ###  #  ");


        ArrayList<Integer> index = new ArrayList<>();
        int flag;
        for (char t : text) {
            flag = 0;
            for (int i = 0; i < alphabet.length; i++) {
                if (t == alphabet[i]) {
                    index.add(i);
                    flag = 1;
                }
            }if(flag==0)index.add(alphabet.length-1);
        }

//        System.out.println(text);
//        for (Integer ind : index) {
//            System.out.println(ind);
//        }

        for (int i = 0; i < H; i++) {
            for (Integer ind : index) {
                System.out.print(ascii.get(i).substring(L * ind, L * ind + L));
            }
            System.out.println();
        }

    }
}