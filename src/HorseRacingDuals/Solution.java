package HorseRacingDuals;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int[] powers = new int[N];
        for (int i = 0; i < N; i++) {
            powers[i] = in.nextInt();
        }

        Arrays.sort(powers);

        int minDiff = Math.abs(powers[0] - powers[1]);

        for (int i = 1; i < powers.length - 1; i++) {
            if (Math.abs(powers[i] - powers[i + 1]) < minDiff) {
                minDiff = Math.abs(powers[i] - powers[i + 1]);

            }
        }
        System.out.println(minDiff);
    }
}
